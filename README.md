# Docker image for SubGit

[Docker Hub](https://hub.docker.com/r/jeker/subgit/) | [SubGit Website](https://subgit.com/)

Purpose of this image was to simplify the process of using SubGit. Forked to
update and improve the docker image.

Workdir is set to `/subgit`

## Usage

    docker run \
      -ti \
      --name subgit \
      jeker/subgit \
      subgit <subgit-commands>

Mount Volume into working directory

    docker run \
      -ti \
      --name subgit \
      -v ${PWD}/workdir:/subgit \
      jeker/subgit \
      subgit <subgit-commands>

## Build

    docker build -t jeker/subgit .

# Docker troubleshooting

Use docker command to see if all required containers are up and running:
```
docker ps
```

Check logs of subgit server container:
```
docker logs subgit
```

Sometimes you might just want to review how things are deployed inside
a running container, you can do this by executing a _bash shell_ through
_docker's exec_ command:

```
docker exec -ti subgit /bin/bash
```

History of an image and size of layers:
```
docker history --no-trunc=true jeker/subgit | tr -s ' ' | tail -n+2 | awk -F " ago " '{print $2}'
```

# Authors

- Przemyslaw Ozgo (<linux@ozgo.info>)
- [saintech](https://github.com/saintech)
- Nicolas Jeker
